DROP DATABASE IF EXISTS db_openapi;
CREATE DATABASE db_openapi DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_unicode_ci;

USE db_openapi;

SET names utf8mb4;
