-- OAuth 2.0 相关的表
-- https://github.com/spring-projects/spring-security-oauth/blob/master/spring-security-oauth2/src/test/resources/schema.sql

CREATE TABLE `oauth_client_details` (
  `client_id`               VARCHAR(36) COMMENT '客户端唯一标识 appKey',
  `resource_ids`            VARCHAR(255) COMMENT '客户端所能访问的资源ID集合 多个资源时用逗号（,）分隔，如 unity-resource,mobile-resource',
  `client_secret`           VARCHAR(255) COMMENT '客户端密匙 appSecret',
  `scope`                   VARCHAR(255) COMMENT '客户端申请的权限范围 可选值包括 read、write、trust',
  `authorized_grant_types`  VARCHAR(255) COMMENT '客户端支持的授权类型 逗号分隔，可选值包括 authorization_code、password、refresh_token、implicit、client_credentials',
  `web_server_redirect_uri` VARCHAR(255) COMMENT '客户端的重定向URI 用于授权类型为 authorization_code 或 implicit 时，接收 code 或 access_token',
  `authorities`             VARCHAR(255) COMMENT '指定客户端所拥有的 SpringSecurity 的权限值',
  `access_token_validity`   INTEGER COMMENT '客户端access_token有效时间 （单位：秒） 默认12小时',
  `refresh_token_validity`  INTEGER COMMENT '客户端refresh_token有效时间 （单位：秒） 默认30天',
  `additional_information`  VARCHAR(4096) COMMENT '预留字段 用于存储客户端的一些其他信息，如国家、地区，注册时的IP地址等',
  `autoapprove`             VARCHAR(255) COMMENT '是否自动Approval 默认值为 false, 可选值包括 true,false,read,write\r\n用于 grant_type=authorization_code 时，用户登录成功后，若该值为 true 或支持的 scope 值，则会跳过用户Approve的页面，直接授权',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE=InnoDB CHARACTER SET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT ='OAuth 客户端信息' ROW_FORMAT=DYNAMIC;

CREATE TABLE oauth_client_token (
  token_id          VARCHAR(36),
  token             BLOB,
  authentication_id VARCHAR(36),
  user_name         VARCHAR(255),
  client_id         VARCHAR(36)
);

CREATE TABLE oauth_access_token (
  token_id          VARCHAR(36),
  token             BLOB,
  authentication_id VARCHAR(36),
  user_name         VARCHAR(255),
  client_id         VARCHAR(36),
  authentication    BLOB,
  refresh_token     VARCHAR(255)
);

CREATE TABLE oauth_refresh_token (
  token_id       VARCHAR(36),
  token          BLOB,
  authentication BLOB
);

CREATE TABLE oauth_code (
  code           VARCHAR(255),
  authentication BLOB
);

-- secret: AENgtl6Y2muoJMcEVJEfSj+cYG+dgRQrDYWqcw4jLFcOmrvOMzmFzcMjYPc/eeOf
INSERT INTO oauth_client_details
VALUES ('wx0023e4fd24e4c17x', 'oauth2-resource', '{bcrypt}$2a$10$SwEvTsU5Yn0t2j72vnLGYeyc1yjSydJnQgnqAvUURf2TdsEPC4m0i',
 'read,write,trust', 'authorization_code,client_credentials,refresh_token,password,implicit', 'http://localhost:8080/me/code',
 'ROLE_ADMIN,ROLE_USER', 7200, 5184000, '{"country":"CN","country_code":"086"}', 'false');

COMMIT;

-- 接入商户表
DROP TABLE IF EXISTS `t_api_access_user`;
CREATE TABLE `t_api_access_user` (
  `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_no`       VARCHAR(32)  NOT NULL COMMENT '用户账号（商户号）',
  `password`      VARCHAR(255) NOT NULL COMMENT '登录密码',
  `name`          VARCHAR(255) NOT NULL COMMENT '接入商户名称',
  `email`         VARCHAR(255) COMMENT '邮箱',
  `tel_no`        VARCHAR(255) COMMENT '电话号码',
  `addr`          VARCHAR(255) COMMENT '办公地址',
  `url`           VARCHAR(255) COMMENT '网址',
  `public_key`    TEXT COMMENT '公钥',
  `private_key`   TEXT COMMENT '私钥',
  `remark`        VARCHAR(255) COMMENT '备注',
  `status`        INT(1)       NOT NULL DEFAULT 1 COMMENT '状态 0-禁用 1-正常 2-初始化',
  `creator`       VARCHAR(32)  NOT NULL COMMENT '创建人',
  `create_time`   DATETIME     NOT NULL DEFAULT NOW() COMMENT '创建时间',
  `modifier`      VARCHAR(32) COMMENT '修改人',
  `modified_time` DATETIME COMMENT '修改时间',
  CONSTRAINT `pk_access_user` PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_access_user`(`user_no`) USING BTREE
) ENGINE=InnoDB CHARACTER SET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='接入商户表' ROW_FORMAT=Dynamic;

-- password: 1234rewq
INSERT INTO t_api_access_user
VALUES (1, 'access-user-01', '$2a$10$yA640xCvSImg5AD2AQU71u2sGFJUGd721jyzHIGhiIyr04yHQgeMq', 'ijleex-me', 'hello@me.com',
  '010-123456', '北京市海淀区东冉村宝蓝·金园国际中心 B3-015', 'http://', NULL, NULL, '密码 1234rewq', 1, 'me', '2018-12-25 14:00:50', NULL, NULL);
INSERT INTO t_api_access_user
VALUES (2, 'access-user-02', '$2a$10$yA640xCvSImg5AD2AQU71u2sGFJUGd721jyzHIGhiIyr04yHQgeMq', 'ijleex-me', 'hello@me.com',
  '010-123456', '北京市海淀区永景园 5-7-602', 'http://100143', NULL, NULL, '密码 1234rewq', 1, 'me', '2018-12-29 11:27:23', NULL, NULL);

DROP TABLE IF EXISTS `t_api_user_authority`;
CREATE TABLE `t_api_user_authority` (
  id            BIGINT(20)  NOT NULL AUTO_INCREMENT COMMENT 'ID',
  user_no       VARCHAR(32) NOT NULL COMMENT '用户号',
  authority     VARCHAR(50) NOT NULL COMMENT '权限',
  creator       VARCHAR(20) COMMENT '创建人',
  create_time   DATETIME COMMENT '创建时间',
  modifier      VARCHAR(20) COMMENT '修改人',
  modified_time DATETIME COMMENT '修改时间',
  CONSTRAINT `pk_user_authority` PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_user_authority`(`user_no`, `authority`) USING BTREE COMMENT '用户授权索引'
) ENGINE=InnoDB CHARACTER SET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户授权' ROW_FORMAT=Dynamic;

-- 两列唯一索引
SET @x := (SELECT COUNT(*) FROM information_schema.statistics
  WHERE table_name = 't_api_user_authority' AND index_name = 'idx_user_authority' AND table_schema = database());
SET @sql := if( @x > 0, 'select ''Index exists.''', 'CREATE UNIQUE INDEX idx_user_authority ON t_api_user_authority(user_no, authority)');
PREPARE stmt FROM @sql;
EXECUTE stmt;

-- 添加外键
ALTER TABLE t_api_user_authority ADD CONSTRAINT `fk_user_authority` FOREIGN KEY (`user_no`)
  REFERENCES t_api_access_user (`user_no`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

INSERT INTO t_api_user_authority(user_no, authority) VALUES ('access-user-01', 'ROLE_ADMIN');
INSERT INTO t_api_user_authority(user_no, authority) VALUES ('access-user-01', 'ROLE_USER');
INSERT INTO t_api_user_authority(user_no, authority) VALUES ('access-user-02', 'ROLE_USER');

COMMIT;

