/*
 * Copyright 2011-2018 ijym-lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.ijleex.openapi.oauth.test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.BytesEncryptor;
import org.springframework.security.crypto.encrypt.Encryptors;

/**
 * SecurityTest
 *
 * @author liym
 * @since 2018-12-25 00:28 新建
 */
public class SecurityTest {

    @Test
    public void testBase6401() {
        String appId = "wx0023e4fd24e4c17x";
        String secret = "AENgtl6Y2muoJMcEVJEfSj+cYG+dgRQrDYWqcw4jLFcOmrvOMzmFzcMjYPc/eeOf";
        String data = appId + ':' + secret;
        String cipher = Base64.getEncoder().encodeToString(data.getBytes(StandardCharsets.UTF_8));
        System.out.println(cipher);

        byte[] bytes = Base64.getDecoder().decode(cipher);
        System.out.println(new String(bytes));
    }

    /**
     * @since 2018-12-25 10:42
     */
    @Test
    public void testBCrypt02() {
        // 客户端重要的属性
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        String secret = "AENgtl6Y2muoJMcEVJEfSj+cYG+dgRQrDYWqcw4jLFcOmrvOMzmFzcMjYPc/eeOf";
        String encodedSecret = encoder.encode(secret);
        System.out.println(encodedSecret);

        // 保存的时候，要添加前缀：{bcrypt}
        boolean matches = encoder.matches(secret, encodedSecret);
        System.out.println(matches);

        // 客户账号
        // String userNo = "access-user-01";
        String password = "1234rewq";
        String encodedPassword = encoder.encode(password);
        System.out.println(encodedPassword);
    }

    /**
     * 基于口令加密（Password Base Encryption）
     *
     * @since 2018-12-29 10:17
     */
    @Test
    public void testEncrypt03() {
        String userNo = "access-user-01";
        String password = "1234rewq";

        String key = "1234567890abcdef";
        String salt = UUID.randomUUID().toString().replaceAll("-", "");

        // BytesEncryptor encryptor = Encryptors.standard(key + "#" + userNo, salt);
        BytesEncryptor encryptor = Encryptors.stronger(key + "#" + userNo, salt);

        byte[] encryptedBytes = encryptor.encrypt(password.getBytes(StandardCharsets.UTF_8));

        Base64.Encoder encoder = Base64.getEncoder();
        String cipherPwd = encoder.encodeToString(encryptedBytes);
        System.out.println(cipherPwd);

        byte[] decryptBytes = encryptor.decrypt(encryptedBytes);
        System.out.println(new String(decryptBytes));
    }

}
