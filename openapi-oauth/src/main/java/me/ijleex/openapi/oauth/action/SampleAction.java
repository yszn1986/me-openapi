/*
 * Copyright 2011-2018 ijym-lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.ijleex.openapi.oauth.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringBootVersion;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * SampleAction
 *
 * @author liym
 * @since 2018-12-25 00:17 新建
 */
@RestController
@RequestMapping("/me")
public class SampleAction {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Hello Spring-Boot
     *
     * @param request 当前请求
     * @return msg
     */
    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String hello(HttpServletRequest request, @RequestHeader("Authorization") String auth) {
        this.logger.debug("hello request: {}", request);
        String version = SpringBootVersion.getVersion();
        this.logger.debug("hello Authorization: {}", auth);
        return "Hello Spring-Boot v" + version;
    }

    /**
     * 授权端点回调地址（https://www.cnblogs.com/cjsblog/p/9184173.html）
     * <p>
     * http://localhost:8080/oauth/authorize?response_type=code&client_id=wx0023e4fd24e4c17x&redirect_uri=http://localhost:8080/me/code&scope=read&state=me-state
     *
     * @param request 请求
     * @param code 授权码，稍后用此授权码交换访问令牌
     * @param state 用于应用存储特定的请求数据，可以防止CSRF攻击（<b>推荐</b>）
     * @return code
     * @since 2018-12-27 13:57
     */
    @RequestMapping(path = "/code", method = RequestMethod.GET)
    public String oauthCode(HttpServletRequest request, String code, String state) {
        this.logger.debug("response code: {}, state: {}", code, state);
        return "code: " + code + ", state: " + state;
    }

}
